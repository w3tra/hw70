import * as actionTypes from '../actionTypes';

const initialState = {
  calc: '',
  total: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SEND_ITEM:
      return {...state, total: state.total + action.amount};
    case actionTypes.REMOVE:
      return {...state, total: state.total.slice(0, state.total.length - 1)};
    case actionTypes.CALCULATE:
      try {
        return {...state, calc: eval(state.total)};
      } catch (e) {
        return {calc: "wrong operation", total: ''};
      }
    case actionTypes.GET_ZERO:
      return {total: ''};
    default:
      return state;
  }
};



export default reducer;